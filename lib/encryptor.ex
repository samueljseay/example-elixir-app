defmodule PublicKeyEncrypt.Encryptor do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def encrypt(data) do
    GenServer.call __MODULE__, {:encrypt, data}
  end

  def handle_call({:encrypt, data}, _, _) do
    encrypted_data =
      File.read!("keys/public_key")
      |> encrypt(data)

    {:reply, encrypted_data, :ok}
  end

  defp encrypt(key, data) when is_binary(key) and is_binary(data) do
    :timer.sleep(2000)
    data
  end
end
