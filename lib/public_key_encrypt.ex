defmodule PublicKeyEncrypt do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(PublicKeyEncrypt.Encryptor, [])
    ]

    opts = [strategy: :one_for_one, name: PublicKeyEncrypt.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
