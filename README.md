# PublicKeyEncrypt

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `public_key_encrypt` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:public_key_encrypt, "~> 0.1.0"}]
    end
    ```

  2. Ensure `public_key_encrypt` is started before your application:

    ```elixir
    def application do
      [applications: [:public_key_encrypt]]
    end
    ```

